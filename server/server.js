import express from 'express'
import cors from 'cors'
import path from 'path'

import { dbConnect } from './db/connect.js'
import { seedDb } from './db/seed/seed.js'
import { config } from './src/config/config.js'
import { routes } from './src/api/routes.js'

const __dirname = path.resolve()

dbConnect()

seedDb()

const app = express()
app.use(cors())

app.use('/static', express.static(path.join(__dirname, 'songs-demo')))
app.use('/api', routes)

app.listen(config.port, () => console.log(`App is listening on port ${config.port}!`))
