import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'

const Schema = mongoose.Schema

const SongSchema = new Schema({
  name: { type: String, required: true },
  size: { type: Number, required: true },
  duration: { type: Number, required: true },
  contentType: { type: String, required: true },
  extension: { type: String, required: true },
})

SongSchema.plugin(mongoosePaginate)

export const Song = mongoose.model('Song', SongSchema)
