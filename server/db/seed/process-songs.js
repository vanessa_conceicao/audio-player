import fs from 'fs'
import path from 'path'
import mp3Duration from 'mp3-duration'
import mime from 'mime-types'

const __dirname = path.resolve()
const songsDir = `${__dirname}/songs-demo`

const getInfo = async (song) => {
  const extension = path.extname(song)
  const size = fs.statSync(`${songsDir}/${song}`).size
  const contentType = mime.lookup(song)
  const name = path.basename(song, extension)
  const duration = await mp3Duration(`${songsDir}/${song}`)

  return {
    name,
    size,
    duration,
    contentType,
    extension,
  }
}

export const getListOfSongs = () => {
  const songs = fs.readdirSync(songsDir).filter((song) => path.extname(song))

  return songs
}

const getSongsInfo = (songs) => {
  const songsPromises = songs.map((song) => getInfo(song))

  return Promise.all(songsPromises)
}

export const processSongs = async () => {
  const songs = await getListOfSongs()
  const songsList = await getSongsInfo(songs)

  return songsList
}
