import path from 'path'

import { Song } from '../models/song.js'
import { processSongs, getListOfSongs } from './process-songs.js'

export const seedDb = async () => {
  const dbSongs = await Song.find().countDocuments()

  if (dbSongs === 0) {
    await populateDb()
    return
  }

  const localSongs = await getListOfSongs()

  if (localSongs.length !== dbSongs) {
    console.log('clear db: number of local and stored songs is different')
    await resetDb()
    return
  }

  checkIfAllLocalSongsAreStored(localSongs)
}

const checkIfAllLocalSongsAreStored = (localSongs) => {
  localSongs.forEach(async (localSong) => {
    const extension = path.extname(localSong)
    const name = path.basename(localSong, extension)
    const dbSong = await Song.find({ name: name, extension: extension })
    if (!dbSong.length) {
      console.log('clear db: local song not stored')
      await resetDb()
      return
    }
  })
}

const resetDb = async () => {
  await clearDb()
  await populateDb()
}

const clearDb = async () => {
  await Song.deleteMany({})
}

const populateDb = async () => {
  const songs = await processSongs()

  Song.create(songs, (err, small) => {
    console.log('Seeding db', err, small)
  })
}
