import { config } from '../src/config/config.js'
import mongoose from 'mongoose'

export const dbConnect = () => mongoose.connect(config.databaseURL, { useNewUrlParser: true })
