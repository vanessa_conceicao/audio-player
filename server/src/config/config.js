export const config = {
  databaseURL: process.env.DATABASE_URL || 'mongodb://localhost:27017/audio-player',
  port: process.env.DB_CONN_PORT || '8080',

  databaseTestURL: process.env.DATABASE_URL_TEST || 'mongodb://localhost:27017/test-audio-player',
  testPort: process.env.DB_CONN_PORT_TEST || '8081',
}
