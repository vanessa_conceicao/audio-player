import { getSongInfo } from '../../services/songs/get-song.js'
import { presenter } from '../../services/presenter.js'

export const details = async (req, res) => {
  const songId = req.params.id

  const song = await getSongInfo(songId)

  if (!song) {
    return res.sendStatus(404)
  }

  return res.status(200).send(presenter(song))
}
