import { getSongs } from '../../services/songs/get-songs.js'
import { listPresenter } from '../../services/presenter.js'

export const list = async (req, res) => {
  const page = req.query.page || 1

  const songs = await getSongs(page)

  return res.status(200).send({ list: listPresenter(songs), total: songs.total })
}
