import express from 'express'

import { list } from './songs/list.js'
import { details } from './songs/details.js'

const router = express.Router()

router.get('/songs', list)
router.get('/songs/:id', details)

export const routes = router
