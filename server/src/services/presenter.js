export const listPresenter = (songs) => {
  return songs.docs.map((song) => {
    const newSong = {
      ...song._doc,
      id: song._id,
    }

    delete newSong._id

    return newSong
  })
}

export const presenter = (song) => {
  return {
    ...song._doc,
    id: song._id,
  }
}
