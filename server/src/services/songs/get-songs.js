import { Song } from '../../../db/models/song.js'

const itemsPerPage = 3

export const getSongs = async (page) => {
  const songs = await Song.paginate({}, { page: page, limit: itemsPerPage }, (_, result) => {
    return result
  })

  return songs
}
