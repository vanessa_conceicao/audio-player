import { Song } from '../../../db/models/song.js'

export const getSongInfo = async (songId) => await Song.findById(songId)
