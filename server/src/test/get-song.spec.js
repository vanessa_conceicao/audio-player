/* eslint-disable no-undef */

import { expect } from 'chai'
import mongoose from 'mongoose'

import { Song } from '../../db/models/song.js'
import { getSongInfo } from '../services/songs/get-song.js'

const songFixture = {
  _id: '5d74043322086f138426be5d',
  name: 'Song 1',
  size: 1234,
  duration: 180,
  contentType: 'audio/mpeg',
  extension: '.mp3',
}

describe('services/songs/get-song', () => {
  beforeEach((done) => {
    Song.create(songFixture).then(() => done())
  })

  it('gets song info', async () => {
    getSongInfo(mongoose.Types.ObjectId('5d74043322086f138426be5d')).then((songInfo) => {
      expect(songInfo).to.not.be.null
    })
  })

  it('returns null for not found song', async () => {
    getSongInfo(mongoose.Types.ObjectId('5d74043322086f138426be5f')).then((songInfo) => {
      expect(songInfo).to.be.null
    })
  })
})
