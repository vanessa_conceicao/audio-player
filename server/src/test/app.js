import express from 'express'
import cors from 'cors'

import { config } from '../../src/config/config.js'
import { routes } from '../../src/api/routes.js'

const app = express()
app.use(cors())

app.use('/api', routes)

app.listen(config.testPort, () => console.log(`App is listening on port ${config.testPort}!`))

export { app }
