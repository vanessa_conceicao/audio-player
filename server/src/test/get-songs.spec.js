/* eslint-disable no-undef */

import { expect } from 'chai'

import { Song } from '../../db/models/song.js'
import { getSongs } from '../services/songs/get-songs.js'

const songsFixture = [
  {
    name: 'Song 1',
    size: 1234,
    duration: 180,
    contentType: 'audio/mpeg',
    extension: '.mp3',
  },
  {
    name: 'Song 2',
    size: 5678,
    duration: 200,
    contentType: 'audio/mpeg',
    extension: '.mp3',
  },
]

describe('services/songs/get-songs', () => {
  beforeEach((done) => {
    Song.create(songsFixture).then(() => done())
  })

  it('gets songs list', async () => {
    getSongs(1).then((songs) => {
      expect(songs).to.not.be.empty
      expect(songs.total).to.be.equal(2)
    })
  })
})
