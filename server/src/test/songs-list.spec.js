/* eslint-disable no-undef */

import chai from 'chai'
import chaiHttp from 'chai-http'

import { app } from './app.js'
import { Song } from '../../db/models/song.js'

chai.use(chaiHttp)
chai.should()

const songsFixture = [
  {
    name: 'Song 1',
    size: 1234,
    duration: 180,
    contentType: 'audio/mpeg',
    extension: '.mp3',
  },
  {
    name: 'Song 2',
    size: 5678,
    duration: 200,
    contentType: 'audio/mpeg',
    extension: '.mp3',
  },
]

describe('api/songs', () => {
  beforeEach((done) => {
    Song.create(songsFixture).then(() => done())
  })

  it('gets all songs', (done) => {
    chai
      .request(app)
      .get('/api/songs')
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        done()
      })
  })
})
