/* eslint-disable no-undef */

import chai from 'chai'
import chaiHttp from 'chai-http'
import mongoose from 'mongoose'

import { app } from './app.js'
import { Song } from '../../db/models/song.js'

chai.use(chaiHttp)
chai.should()

const songFixture = {
  _id: '5d74043322086f138426be5f',
  name: 'Song 1',
  size: 1234,
  duration: 180,
  contentType: 'audio/mpeg',
  extension: '.mp3',
}

describe('api/songs/:id', () => {
  beforeEach((done) => {
    Song.create(songFixture).then(() => done())
  })

  it('gets a specif song', (done) => {
    const id = mongoose.Types.ObjectId('5d74043322086f138426be5f')
    chai
      .request(app)
      .get(`/api/songs/${id}`)
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')
        done()
      })
  })

  it('returns not found code', (done) => {
    const id = mongoose.Types.ObjectId('5d7414a6c19f4f1b144194a5')
    chai
      .request(app)
      .get(`/api/songs/${id}`)
      .end((err, res) => {
        res.should.have.status(404)
        done()
      })
  })
})
