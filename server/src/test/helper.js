/* eslint-disable no-undef */
import mongoose from 'mongoose'
import { config } from '../config/config'

mongoose.connect(config.databaseTestURL, { useNewUrlParser: true })

beforeEach((done) => {
  mongoose.connection.collections.songs.drop(() => {
    done()
  })
})
