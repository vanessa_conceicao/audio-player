const config = {
  baseRoute: process.env.REACT_APP_API_BASE_ROUTE || 'http://localhost:8080/api',
  playRoute: process.env.REACT_APP_API_PLAY_ROUTE || 'http://localhost:8080/static',
}

export default config
