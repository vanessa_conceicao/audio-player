import axios from 'axios'
import config from '../../config/config'

const details = async (songId) => {
  const { data } = await axios({
    method: 'get',
    url: `${config.baseRoute}/songs/${songId}`,
  })

  return data
}

export default details
