import axios from 'axios'
import config from '../../config/config'

const list = async (page) => {
  const { data } = await axios({
    method: 'get',
    url: `${config.baseRoute}/songs?page=${page}`,
  })

  return data
}

export default list
