import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { PlayCircleOutline, PauseCircleOutline, ArrowBack } from '@material-ui/icons'
import { withStyles } from '@material-ui/core/styles'
import { Duration } from 'luxon'

import { detailsAction } from '../redux/songs/details'
import { loadAction, playAction, pauseAction } from '../redux/player/player'

class SongDetails extends Component {
  componentDidMount() {
    const {
      songInfo,
      getSongInfo,
      match: { params },
    } = this.props

    if (!Object.keys(songInfo).length) {
      getSongInfo(params.id)
    }
  }

  play = () => {
    const { playSong, songPlayingId, songInfo, loadSong } = this.props
    if (songPlayingId !== songInfo.id) {
      loadSong(songInfo)
    } else {
      playSong()
    }
  }

  pause = () => {
    const { pauseSong } = this.props
    pauseSong()
  }

  formatSize = (size) => {
    return (size / (1024 * 1024)).toFixed(2)
  }

  formatDuration = (duration) => {
    return (
      duration &&
      Duration.fromMillis(duration * 1000)
        .shiftTo('minutes')
        .toFormat('mm:ss')
    )
  }

  goBack = () => {
    const { history } = this.props

    history.goBack()
  }

  render() {
    const { songInfo, songPlayingId, playing, loadingDetails, error, classes } = this.props

    if (loadingDetails) {
      return <div>Loading...</div>
    }

    if (error) {
      return <div>Song not found</div>
    }

    return (
      <div>
        <div className={classes.container}>
          <div>
            <p>
              {songInfo.name}
              {songInfo.extension}
            </p>
            <p>Duration: {this.formatDuration(songInfo.duration)}</p>
            <p>Size: {this.formatSize(songInfo.size)} MB</p>
            <ArrowBack onClick={this.goBack} />
          </div>
          {songPlayingId === songInfo.id && playing ? (
            <PauseCircleOutline className={classes.icon} onClick={this.pause} />
          ) : (
            <PlayCircleOutline className={classes.icon} onClick={this.play} />
          )}
        </div>
      </div>
    )
  }
}

SongDetails.propTypes = {
  songInfo: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      size: PropTypes.number,
      duration: PropTypes.number,
      contentType: PropTypes.string,
      extension: PropTypes.string,
    }),
  ),
  error: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object])),
  getSongInfo: PropTypes.func,
  loadSong: PropTypes.func,
  loadingDetails: PropTypes.bool,
  match: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.object])),
  classes: PropTypes.objectOf(PropTypes.string),
  songPlayingId: PropTypes.string,
  playing: PropTypes.bool,
  pauseSong: PropTypes.func,
  playSong: PropTypes.func,
  history: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.func]),
  ),
}

const mapStateToPros = (state) => ({
  ...state.songs,
  ...state.player,
})

const mapDispatchToProps = (dispatch) => ({
  getSongInfo: bindActionCreators(detailsAction, dispatch),
  loadSong: bindActionCreators(loadAction, dispatch),
  playSong: bindActionCreators(playAction, dispatch),
  pauseSong: bindActionCreators(pauseAction, dispatch),
})

const styles = () => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icon: {
    cursor: 'pointer',
    fontSize: 50,
  },
})

export default connect(
  mapStateToPros,
  mapDispatchToProps,
)(withStyles(styles)(SongDetails))
