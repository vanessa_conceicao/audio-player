import './loader.css'

import * as React from 'react'
import { Loop } from '@material-ui/icons'

const Loader = () => <Loop className="loading loading--spin" fontSize="large" />

export default Loader
