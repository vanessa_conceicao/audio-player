import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { NavigateNext, NavigateBefore, PlayCircleOutline, PauseCircleOutline } from '@material-ui/icons'
import { Link, List, ListItem } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import { listAction } from '../redux/songs/list'
import { loadAction, playAction, pauseAction } from '../redux/player/player'
import { setSongInfo } from '../redux/songs/details'
import Loader from './Loader'

const itemsPerPage = 3

class Songs extends Component {
  componentDidMount() {
    const { listSongs, history, currentPage } = this.props

    const pageNumber = this.getSearchPage() || currentPage || 1

    listSongs({ page: pageNumber })
    history.push(`/songs?page=${pageNumber}`)
  }

  play = (song) => {
    const { playSong, songPlayingId, loadSong } = this.props
    if (songPlayingId !== song.id) {
      loadSong(song)
    } else {
      playSong()
    }
  }

  pause = () => {
    const { pauseSong } = this.props
    pauseSong()
  }

  getLastPage = () => {
    const { total } = this.props

    return Math.ceil(total / itemsPerPage)
  }

  getSearchPage = () => {
    const { location } = this.props
    const searchParams = new URLSearchParams(location.search)

    return +searchParams.get('page')
  }

  nextPage = () => {
    const { listSongs, currentPage, history } = this.props

    const hasNextPage = currentPage + 1 <= this.getLastPage()

    if (hasNextPage) {
      listSongs({ page: currentPage + 1 })
      history.push(`/songs?page=${currentPage + 1}`)
    }
  }

  previousPage = () => {
    const { listSongs, currentPage, history } = this.props

    if (currentPage > 1) {
      listSongs({ page: currentPage - 1 })
      history.push(`/songs?page=${currentPage - 1}`)
    }
  }

  openSongDetails = (song) => {
    const { setCurrentInfo, history } = this.props
    setCurrentInfo(song)
    history.push(`/songs/${song.id}`)
  }

  render() {
    const { songsList, songPlayingId, playing, loadingSongs, classes } = this.props

    if (loadingSongs) {
      return (
        <div className={classes.container}>
          <Loader />
        </div>
      )
    }

    return (
      <div className={classes.container}>
        <List>
          {songsList.map((song) => (
            <ListItem key={`song-${song.id}`} className={classes.listItem}>
              {songPlayingId === song.id && playing ? (
                <PauseCircleOutline className={classes.icon} onClick={this.pause} />
              ) : (
                <PlayCircleOutline className={classes.icon} onClick={() => this.play(song)} />
              )}

              <Link className={classes.link} onClick={() => this.openSongDetails(song)}>
                {song.name}
                {song.extension}
              </Link>
            </ListItem>
          ))}
        </List>
        <div className={classes.navigate}>
          <NavigateBefore className={classes.icon} onClick={this.previousPage} />
          <NavigateNext className={classes.icon} onClick={this.nextPage} />
        </div>
      </div>
    )
  }
}

Songs.propTypes = {
  songsList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      size: PropTypes.number,
      duration: PropTypes.number,
      contentType: PropTypes.string,
      extension: PropTypes.string,
    }),
  ),
  total: PropTypes.number,
  history: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.func]),
  ),
  location: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object])),
  listSongs: PropTypes.func,
  loadSong: PropTypes.func,
  setCurrentInfo: PropTypes.func,
  loadingSongs: PropTypes.bool,
  currentPage: PropTypes.number,
  classes: PropTypes.objectOf(PropTypes.string),
  songPlayingId: PropTypes.string,
  playing: PropTypes.bool,
  pauseSong: PropTypes.func,
  playSong: PropTypes.func,
}

const mapStateToPros = (state) => ({
  ...state.songs,
  ...state.player,
})

const mapDispatchToProps = (dispatch) => ({
  listSongs: bindActionCreators(listAction, dispatch),
  loadSong: bindActionCreators(loadAction, dispatch),
  setCurrentInfo: bindActionCreators(setSongInfo, dispatch),
  playSong: bindActionCreators(playAction, dispatch),
  pauseSong: bindActionCreators(pauseAction, dispatch),
})

const styles = () => ({
  container: {
    minHeight: 136,
    display: 'flex',
    justifyContent: 'space-between',
  },
  listItem: {
    display: 'flex',
    paddingLeft: 0,
  },
  link: {
    fontSize: 16,
    color: 'black',
    cursor: 'pointer',
  },
  navigate: {
    marginBottom: 5,
    alignSelf: 'flex-end',
  },
  icon: {
    cursor: 'pointer',
    marginRight: 5,
  },
})

export default connect(
  mapStateToPros,
  mapDispatchToProps,
)(withStyles(styles)(Songs))
