import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles'
import { Card, CardContent, CardMedia, IconButton, Typography } from '@material-ui/core'
import { PlayArrow, Pause, Stop } from '@material-ui/icons'

import { playAction, pauseAction, stopAction } from '../redux/player/player'

class Player extends Component {
  constructor(props) {
    super(props)
    this.player = React.createRef()
  }

  componentDidUpdate() {
    const { playing, changedSong } = this.props
    if (playing && changedSong) {
      this.player.current.load()
      this.player.current.play()
    }

    if (playing && !changedSong) {
      this.player.current.play()
    }

    if (!playing) {
      this.player.current.pause()
    }
  }

  play = () => {
    const { playSong } = this.props
    playSong()
    this.player.current.play()
  }

  pause = () => {
    const { pauseSong } = this.props
    pauseSong()
    this.player.current.pause()
  }

  render() {
    const { classes, playing, songSource, songName, stopSong } = this.props

    return (
      <Card className={classes.container}>
        <CardContent className={classes.content}>
          <Typography className={classes.text} component="h6" variant="h6">
            {songName || 'Choose a song'}
          </Typography>
          <div className={classes.controls}>
            {songSource && (
              <IconButton onClick={playing ? this.pause : this.play}>
                {playing ? <Pause className={classes.icon} /> : <PlayArrow className={classes.icon} />}
              </IconButton>
            )}
            <IconButton onClick={stopSong}>
              <Stop className={classes.icon} />
            </IconButton>
          </div>
        </CardContent>
        <CardMedia component="img" className={classes.image} image="/images/wallpaper.jpg" />

        <audio ref={this.player}>
          <source src={songSource} type="audio/mpeg" />
        </audio>
      </Card>
    )
  }
}

Player.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  playSong: PropTypes.func,
  pauseSong: PropTypes.func,
  stopSong: PropTypes.func,
  playing: PropTypes.bool,
  songSource: PropTypes.string,
  songName: PropTypes.string,
  changedSong: PropTypes.bool,
}

const mapStateToPros = (state) => ({
  ...state.player,
})

const mapDispatchToProps = (dispatch) => ({
  playSong: bindActionCreators(playAction, dispatch),
  pauseSong: bindActionCreators(pauseAction, dispatch),
  stopSong: bindActionCreators(stopAction, dispatch),
})

const styles = () => ({
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#020b17',
    height: 160,
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  text: {
    color: 'white',
  },
  image: {
    width: 100,
  },
  controls: {
    display: 'flex',
  },
  icon: {
    color: 'white',
    height: 40,
    width: 40,
  },
})

export default connect(
  mapStateToPros,
  mapDispatchToProps,
)(withStyles(styles)(Player))
