import './app.css'

import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Paper } from '@material-ui/core'

import Player from './Player'
import Songs from './Songs'
import SongDetails from './SongDetails'

const App = () => (
  <div className="page">
    <Paper className="home">
      <Player />
      <Switch>
        <Route exact path={['/', '/songs']} component={Songs} />
        <Route path="/songs/:id" component={SongDetails} />
        <Route component={Songs} />
      </Switch>
    </Paper>
  </div>
)

export default App
