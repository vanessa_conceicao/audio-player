export default {
  playing: false,
  paused: false,
  changedSong: false,
  songSource: undefined,
  songName: undefined,
  songPlayingId: undefined,
}
