import { handleActions } from 'redux-actions'

import initialState from './initial-state'
import { playerActions } from './player'

const playerReducer = handleActions(playerActions, initialState)

export default playerReducer
