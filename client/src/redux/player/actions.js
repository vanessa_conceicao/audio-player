export default {
  play: 'PLAY_SONG',
  pause: 'PAUSE_SONG',
  stop: 'STOP_SONG',
  load: 'LOAD_SONG',
}
