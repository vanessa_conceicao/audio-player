import { createAction } from 'redux-actions'

import actions from './actions'
import config from '../../config/config'
import initialState from './initial-state'

export const playAction = createAction(actions.play)
export const pauseAction = createAction(actions.pause)
export const loadAction = createAction(actions.load)
export const stopAction = createAction(actions.stop)

export const playerActions = {
  [actions.load]: (state, { payload }) => ({
    ...state,
    songPlayingId: payload.id,
    songName: payload.name,
    songSource: `${config.playRoute}/${payload.name}${payload.extension}`,
    playing: true,
    paused: false,
    changedSong: true,
  }),

  [actions.play]: (state) => ({
    ...state,
    playing: true,
    paused: false,
    changedSong: false,
  }),

  [actions.pause]: (state) => ({
    ...state,
    playing: false,
    paused: true,
    changedSong: false,
  }),

  [actions.stop]: () => ({
    ...initialState,
  }),
}
