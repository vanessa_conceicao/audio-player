import { applyMiddleware, createStore, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import songs from './songs'
import player from './player'

const reducers = combineReducers({
  songs,
  player,
})

const configureStore = () => createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))

export default configureStore
