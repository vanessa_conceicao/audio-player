import { createActionThunk } from 'redux-thunk-actions'

import list from '../../api/songs/list'
import actions from './actions'

export const listAction = createActionThunk(actions.list, ({ page }) => list(page))

export const listActions = {
  [listAction.STARTED]: (state, { payload }) => ({
    ...state,
    loadingSongs: true,
    currentPage: payload[0].page,
  }),
  [listAction.SUCCEEDED]: (state, { payload }) => ({
    ...state,
    songsList: payload.list,
    total: payload.total,
  }),
  [listAction.FAILED]: (state, { payload }) => ({
    ...state,
    songsList: [],
    loadingSongs: false,
    total: 0,
    error: payload,
  }),
  [listAction.ENDED]: (state) => ({
    ...state,
    loadingSongs: false,
  }),
}
