export default {
  list: 'SONGS_LIST',
  details: 'SONG_INFO',
  setSongInfo: 'SET_SONG_INFO',
}
