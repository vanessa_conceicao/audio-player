import { handleActions } from 'redux-actions'

import initialState from './initial-state'
import { listActions } from './list'
import { detailsActions } from './details'

const songsActions = { ...listActions, ...detailsActions }

const songsReducer = handleActions(songsActions, initialState)

export default songsReducer
