import { createActionThunk } from 'redux-thunk-actions'
import { createAction } from 'redux-actions'

import details from '../../api/songs/details'
import actions from './actions'

export const detailsAction = createActionThunk(actions.details, (songId) => details(songId))
export const setSongInfo = createAction(actions.setSongInfo)

export const detailsActions = {
  [detailsAction.STARTED]: (state) => ({
    ...state,
    loadingDetails: true,
  }),
  [detailsAction.SUCCEEDED]: (state, { payload }) => ({
    ...state,
    songInfo: payload,
  }),
  [detailsAction.FAILED]: (state, { payload }) => ({
    ...state,
    songInfo: [],
    loadingDetails: false,
    error: payload,
  }),
  [detailsAction.ENDED]: (state) => ({
    ...state,
    loadingDetails: false,
  }),

  [actions.setSongInfo]: (state, { payload }) => ({
    ...state,
    songInfo: payload,
  }),
}
