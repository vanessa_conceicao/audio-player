export default {
  songsList: [],
  total: 0,
  currentPage: undefined,
  loadingSongs: false,
  loadingDetails: false,
  error: undefined,
  songInfo: {},
}
