# Audio Player

Song player that downloads songs and its metadata from a server, show them in a list view with the ability to play them on a web browser.

## Getting Started

To get started you can clone this repo and run `yarn` inside `/client` and `/server` folders to install the project dependencies.

### API

To start the project, open the server folder and run:

`yarn start`

Server will be running on [http://localhost:8080](http://localhost:8080/).

### Client

To start the project, open the client folder and run:

`yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Prerequisites

To run this project you will need to:

- Have [Mongo DB](https://docs.mongodb.com/manual/administration/install-community/) installed and running

- Use `node 12.8.1`

## Running tests

Inside `/server` run `yarn test`

## Built With

- [Redux actions](https://github.com/redux-utilities/redux-actions) and [Redux thunk actions](https://github.com/machadogj/redux-thunk-actions) - Used to easily create action creators and get it converted into a reducer, making the redux implementation less verbose.
- [Redux dev tools](https://github.com/zalmoxisus/redux-devtools-extension) - To add the ability to visualize, via dev tools extension on a web browser, all the events that are happening in a redux application.
- [Material UI styling](https://material-ui.com/styles/basics/) - To create and style the views.

## Improvements

- Add a progress bar to the player.
- Implementing previous and skip songs buttons.
- Improve errors treatment in frontend, e.g., when a file is deleted from `/songs-demo` folder and the api is not restarted.

## Acknowledgments

- Songs downloaded from: [Bensound.com](https://www.bensound.com)
